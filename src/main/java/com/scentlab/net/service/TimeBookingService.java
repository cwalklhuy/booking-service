package com.scentlab.net.service;

import java.util.List;

import com.scentlab.net.payload.response.DateSlots;
import org.springframework.http.ResponseEntity;

import com.scentlab.net.entity.TimeBooking;

public interface TimeBookingService {

    ResponseEntity<List<TimeBooking>> getTimeBookingAvailable(String datePicked);

    ResponseEntity<List<DateSlots>> getTimeBookingOneWeek(String dateFrom, String dateTo);
}
