package com.scentlab.net.service;

import com.scentlab.net.config.ScentLabConstants;
import com.scentlab.net.entity.*;
import com.scentlab.net.entity.json.*;
import com.scentlab.net.payload.request.MakeBookingReg;
import com.scentlab.net.payload.request.UpdateBookingReq;
import com.scentlab.net.payload.response.CustomerBooking;
import com.scentlab.net.payload.response.MessageResponse;
import com.scentlab.net.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookingService {

    private final AccountRepository accountRepository;
    private final DateBookingRepository dateBookingRepository;
    private final TimeBookingRepository timeBookingRepository;
    private final DateTimeBookingRepository dateTimeBookingRepository;
    private final BookingRepository bookingRepository;
    private final ModelMapper modelMapper;

    private final PerfumeRepository perfumeRepository;

    public BookingService(AccountRepository accountRepository, DateBookingRepository dateBookingRepository,
                          TimeBookingRepository timeBookingRepository, DateTimeBookingRepository dateTimeBookingRepository,
                          BookingRepository bookingRepository, PerfumeRepository perfumeRepository, ModelMapper modelMapper) {
        this.accountRepository = accountRepository;
        this.dateBookingRepository = dateBookingRepository;
        this.timeBookingRepository = timeBookingRepository;
        this.dateTimeBookingRepository = dateTimeBookingRepository;
        this.bookingRepository = bookingRepository;
        this.perfumeRepository = perfumeRepository;
        this.modelMapper = modelMapper;
    }

    public List<BookingDTO> getAllBookings() {

        List<Booking> bookingList = bookingRepository.findAll();

        return bookingList.stream().map(this::convertBookingToBookingDTO).collect(Collectors.toList());
    }

    private BookingDTO convertBookingToBookingDTO(Booking booking) {
        BookingDTO bookingDTO = modelMapper.map(booking, BookingDTO.class);
        bookingDTO.setDateBooking(booking.getDateTimeBooking().getDateBooking().getDateCreated().toString());
        bookingDTO.setCustomerProfile(booking.getInfoCustomer().getCustomerProfile());
        bookingDTO.setTimeBooking(booking.getDateTimeBooking().getTimeBooking());
        bookingDTO.setPerfumes(booking.getInfoCustomer().getPerfumePickeds().stream()
                .map(this::convertPerfumePickedIntoPerfumeDTO).collect(Collectors.toList()));

        return bookingDTO;
    }

    private PerfumeDTO convertPerfumePickedIntoPerfumeDTO(PerfumePicked perfume) {

        return modelMapper.map(perfume, PerfumeDTO.class);
    }

    public List<CustomerBooking> findAllBookings() {

        List<Booking> bookingList = bookingRepository.findAll();

        return bookingList.stream().map(CustomerBooking::new).collect(Collectors.toList());
    }

    public ResponseEntity<List<CustomerBooking>> findAllBookingsByCustomerName(String customerName) {
        final Collection<Booking> bookings = bookingRepository.findAllByAccount_Username(customerName);
        if (bookings.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(bookings.stream().map(CustomerBooking::new).collect(Collectors.toList()));
    }

    private List<Note> getNotesByPerfume(List<PerfumeFragrantica> perfumeFragranticas) {

        List<Note> notes = new ArrayList<>();
        perfumeFragranticas.forEach(e -> {
            if (!e.getNotePerfumes().isEmpty()) {
                e.getNotePerfumes().forEach(notePerfume -> notes.add(notePerfume.getNote()));
            }
        });

        return notes;
    }

    public ResponseEntity<?> bookingDone(Integer bookingId) {
        final Booking booking = bookingRepository.findById(bookingId)
                .orElseThrow(() -> new RuntimeException("Booking: " + bookingId + " not found"));
        booking.setStatus(ScentLabConstants.PERFUME_FINISHED);
        final DateTimeBooking dateTimeBooking = booking.getDateTimeBooking();
        dateTimeBooking.setType(ScentLabConstants.DATE_TIME_BOOKING_FINISHED);
        final InfoCustomer infoCustomer = booking.getInfoCustomer();
        LocalDateTime dateTime = LocalDateTime.now();
        String date = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String time = dateTime.format(DateTimeFormatter.ofPattern("HH:mm"));
        infoCustomer.getTrackingBookings().add(new TrackingBooking(date, time, "Nước hoa đã hoàn thành"));
        booking.setInfoCustomer(infoCustomer);
        bookingRepository.save(booking);
        dateTimeBookingRepository.save(dateTimeBooking);

        return ResponseEntity.ok().body(new MessageResponse("Update successfully!!!"));
    }

    public ResponseEntity<?> rating(Integer rating, Integer bookingId) {

        final Booking booking = bookingRepository.findById(bookingId)
                .orElseThrow(() -> new RuntimeException("Data was not found!!!"));
        booking.setRating(rating);

        bookingRepository.save(booking);

        return ResponseEntity.ok().body(new MessageResponse("Rating successfully!!!"));
    }

    public ResponseEntity<CustomerBooking> findBookingByBookingId(Integer bookingId) {
        final Booking booking = bookingRepository.findById(bookingId)
                .orElseThrow(() -> new RuntimeException("Data was not found"));

        return ResponseEntity.ok().body(new CustomerBooking(booking));
    }

    @Transactional
    public ResponseEntity<?> createBooking(MakeBookingReg makeBooking) {

        List<DateBooking> dateBooking = dateBookingRepository.findDateBookingByDateCreated(
                LocalDate.parse(makeBooking.getMakeAppointmentDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        final TimeBooking timeBooking = timeBookingRepository.findById(Integer.valueOf(makeBooking.getTimeBookingId()))
                .orElseThrow(() -> new RuntimeException("The time booking was delete"));
        DateTimeBooking dateTimeBooking = new DateTimeBooking();
        if (!dateBooking.isEmpty()) {
            final DateBooking dateBooking1 = dateBooking.stream().findFirst()
                    .orElseThrow(() -> new RuntimeException("Date Booking not found"));

            final List<DateTimeBooking> dateTimeBookings = dateTimeBookingRepository
                    .findByDateBookingAndTimeBooking(dateBooking1, timeBooking);
            if (!dateTimeBookings.isEmpty()) {
                return ResponseEntity.badRequest().body(new MessageResponse("Date Time has already taken"));
            }
            dateTimeBooking.setDateBooking(dateBooking1);

        } else {
            dateTimeBooking.setDateBooking(DateBooking.builder().dateCreated(
                    LocalDate.parse(makeBooking.getMakeAppointmentDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                    .build());
        }
        dateTimeBooking.setTimeBooking(timeBooking);

        InfoCustomer infoCustomer = createNewInfoCustomer(makeBooking);

        Booking booking = Booking.builder()
                .dateTimeBooking(dateTimeBooking)
                .expertMessage(makeBooking.getExpertMessage())
                .rating(0)
                .type(makeBooking.getType())
                .status(makeBooking.getStatus())
                .infoCustomer(infoCustomer)
                .noteFromExpert(makeBooking.getNoteFromExpert())
                .build();
        if (makeBooking.getUserId() != null) {
            final Account account = accountRepository.findById(makeBooking.getUserId())
                    .orElseThrow(() -> new RuntimeException("Account not found"));
            booking.setAccount(account);
            booking.setCustomerName(account.getFullName());
        } else {
            booking.setCustomerName(makeBooking.getCustomerName());
        }
        bookingRepository.save(booking);

        return ResponseEntity.ok().body(new MessageResponse("Create Successfully!!!"));
    }

    @Transactional
    public ResponseEntity<?> updateBooking(UpdateBookingReq updateBookingReq){
        final Booking booking = bookingRepository.findById(updateBookingReq.getBookingId())
                .orElseThrow(() -> new RuntimeException("The booking was delete"));
        // Kiểm tra có cập nhập timebooking không
        final LocalDate dateBookingCreated = LocalDate.parse(updateBookingReq.getMakeAppointmentDate(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        final List<DateTimeBooking> dateTimeBookings = dateTimeBookingRepository
                .findAllByDateBooking_DateCreatedAndTimeBooking_TimeBookingId(dateBookingCreated,
                        Integer.valueOf(updateBookingReq.getTimeBookingId()));
        // Tạo mới 1 datetimebooking mới dựa trên datetimebooking_id của table booking
        final DateTimeBooking dateTimeBooking = booking.getDateTimeBooking();
        final TimeBooking timeBooking = timeBookingRepository
                .findById(Integer.valueOf(updateBookingReq.getTimeBookingId()))
                .orElseThrow(() -> new RuntimeException("The time was delete"));
        if (dateTimeBookings.isEmpty()) {
            // Check dateBooking có trong db chưa
            final List<DateBooking> dateBookings = dateBookingRepository
                    .findDateBookingByDateCreated(dateBookingCreated);
            if (dateBookings.isEmpty()) {
                // Tạo mới dateBooking
                dateTimeBooking.setDateBooking(
                        DateBooking.builder().dateCreated(LocalDate.parse(updateBookingReq.getMakeAppointmentDate(),
                                DateTimeFormatter.ofPattern("yyyy-MM-dd"))).build());
            } else {
                dateTimeBooking.setDateBooking(dateBookings.get(0));
            }
            dateTimeBooking.setTimeBooking(timeBooking);
            booking.setDateTimeBooking(dateTimeBooking);
        }
        InfoCustomer infoCustomer = updateInfoCustomer(updateBookingReq);
        booking.setInfoCustomer(infoCustomer);
        booking.setNoteFromExpert(updateBookingReq.getNoteFromExpert());
        booking.setStatus(updateBookingReq.getStatus());
        booking.setExpertMessage(updateBookingReq.getExpertMessage());
        bookingRepository.save(booking);

        return ResponseEntity.ok().body(new MessageResponse("Update Successfully!!!"));
    }

    private InfoCustomer createNewInfoCustomer(MakeBookingReg makeBooking) {
        List<Long> listPerfumeId = makeBooking.getPerfumeId();
        final CustomerProfile customerProfile = makeBooking.getCustomerProfile();

        return makeInfoCustomer(listPerfumeId, "create", customerProfile, null, makeBooking.getExpertMessage());
    }

    private InfoCustomer updateInfoCustomer(UpdateBookingReq updateBookingReq) {
        List<Long> listPerfumeId = updateBookingReq.getPerfumeId();
        final CustomerProfile customerProfile = updateBookingReq.getCustomerProfile();
        final Integer bookingId = updateBookingReq.getBookingId();
        final String expertMessage = updateBookingReq.getExpertMessage();

        return makeInfoCustomer(listPerfumeId, "update", customerProfile, bookingId, expertMessage);
    }

    private InfoCustomer makeInfoCustomer(List<Long> listPerfumeId, String state, CustomerProfile customerProfile,
            Integer bookingId, String expertMessage) {

        List<PerfumeFragrantica> perfumeFragranticas = perfumeRepository.findByPerfumeFragranticaIdIn(listPerfumeId);
        List<Note> notes = getNotesByPerfume(perfumeFragranticas);
        List<PerfumePicked> perfumePickeds = new ArrayList<>();

        perfumeFragranticas.forEach(e -> {
            final PerfumePicked perfumePicked = new PerfumePicked(e);
            perfumePickeds.add(perfumePicked);
        });
        Map<String, Integer> map = new HashMap<>();

        notes.forEach(note -> {
            if (map.containsKey(note.getName())) {
                map.put(note.getName(), map.get(note.getName()) + 1);
            } else {
                map.put(note.getName(), 1);
            }
        });
        List<NoteAvg> noteAvgs = map.entrySet().stream().map(e -> new NoteAvg(e.getKey(), e.getValue()))
                .collect(Collectors.toList());

        InfoCustomer infoCustomer = InfoCustomer.builder().customerProfile(customerProfile).noteAvgs(noteAvgs)
                .perfumePickeds(perfumePickeds).build();
        LocalDateTime dateTime = LocalDateTime.now();
        String date = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String time = dateTime.format(DateTimeFormatter.ofPattern("HH:mm"));
        if (state.equals("create")) {
            List<TrackingBooking> trackingBookings = new ArrayList<>();
            trackingBookings.add(new TrackingBooking(date, time, "Đơn hẹn đã được tạo"));
            infoCustomer.setTrackingBookings(trackingBookings);
        } else {
            final List<TrackingBooking> trackingBookings1 = bookingRepository.findById(bookingId)
                    .orElseThrow(() -> new RuntimeException("Data not found")).getInfoCustomer().getTrackingBookings();
            trackingBookings1.add(new TrackingBooking(date, time, expertMessage));
            infoCustomer.setTrackingBookings(trackingBookings1);
        }

        return infoCustomer;
    }
}
