package com.scentlab.net.service;

import com.scentlab.net.entity.Booking;
import com.scentlab.net.payload.response.NotificationExpert;
import com.scentlab.net.repository.BookingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NotificationService {

    private final BookingRepository bookingRepository;

    public NotificationService(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public ResponseEntity<List<NotificationExpert>> getNotificationByUsername(String username) {

        final Collection<Booking> bookings = bookingRepository.findAllByAccount_Username(username);
        List<NotificationExpert> notificationExperts = new ArrayList<>();
        bookings.forEach(booking -> booking.getInfoCustomer().getTrackingBookings().forEach(trackingBooking -> {
            NotificationExpert notificationExpert = new NotificationExpert();
            notificationExpert.setBookingId(booking.getBookingId());
            notificationExpert.setDate(trackingBooking.getDate());
            notificationExpert.setNotification(trackingBooking.getNotificationName());
            notificationExpert.setTime(trackingBooking.getTime());
            notificationExperts.add(notificationExpert);
        }));
        final List<NotificationExpert> notificationExpertsSorted = notificationExperts.stream()
                .sorted()
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(notificationExpertsSorted);
    }
}
