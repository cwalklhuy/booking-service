package com.scentlab.net.service;

import com.scentlab.net.entity.DateBooking;
import com.scentlab.net.entity.DateTimeBooking;
import com.scentlab.net.entity.TimeBooking;
import com.scentlab.net.payload.response.DateSlots;
import com.scentlab.net.payload.response.Slot;
import com.scentlab.net.repository.DateTimeBookingRepository;
import com.scentlab.net.repository.TimeBookingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TimeBookingServiceImpl implements TimeBookingService{
    private final TimeBookingRepository timeBookingRepository;
    private final DateTimeBookingRepository dateTimeBookingRepository;

    public TimeBookingServiceImpl(TimeBookingRepository timeBookingRepository, DateTimeBookingRepository dateTimeBookingRepository) {
        this.timeBookingRepository = timeBookingRepository;
        this.dateTimeBookingRepository = dateTimeBookingRepository;
    }

    @Override
    public ResponseEntity<List<TimeBooking>> getTimeBookingAvailable(final String datePicked) {
        final LocalDate localDate = LocalDate.parse(datePicked);
        final Collection<DateTimeBooking> dateTimeBookings =
                dateTimeBookingRepository.findAllByDateBookingDateCreated(localDate);
        final List<TimeBooking> timeBookings = timeBookingRepository.findAll();

        if (!dateTimeBookings.isEmpty()) {
            final List<TimeBooking> timeBookingsPicked = genTimeBookingPicked(dateTimeBookings);
            timeBookings.removeAll(timeBookingsPicked);
        }
        return ResponseEntity.ok().body(timeBookings);
    }

    @Override
    public ResponseEntity<List<DateSlots>> getTimeBookingOneWeek(String dateFrom, String dateTo) {
        List<DateSlots> dateSlots = new ArrayList<>();
        //Lấy tất cả slot ra
        List<TimeBooking> timeBookings = timeBookingRepository.findAll();
        getListDayofWeek(dateFrom).forEach(e -> {
            List<Slot> slots = new ArrayList<>();
            Collection<DateTimeBooking> dateCreated = dateTimeBookingRepository.findAllByDateBookingDateCreated(e);

            if (!dateCreated.isEmpty()) {
                //Kiểm tra trong datetimebooking
                //Đây là các thời gian đã được book
                final DateBooking dateBooking = dateCreated.stream().map(DateTimeBooking::getDateBooking).findFirst().get();
                timeBookings.forEach(timeBooking -> {
                    final Optional<DateTimeBooking> dateTimeBooking =
                            dateTimeBookingRepository.findByDateBookingIdAndTimeBookingId(dateBooking.getDateBookingId(), timeBooking.getTimeBookingId());
                    if (dateTimeBooking.isPresent()) {
                        slots.add(new Slot(timeBooking.getTimeBookingId(), dateTimeBooking.get().getType()));
                    } else {
                        slots.add(new Slot(timeBooking.getTimeBookingId(), "Sẵn sàng"));
                    }
                });
            } else {
                //Nếu rỗng nghĩa là tất cả các slot ngày đó ok
                timeBookings.forEach(timeBooking -> slots.add(new Slot(timeBooking.getTimeBookingId(), "Sẵn sàng")));
            }
            dateSlots.add(DateSlots.builder()
                    .date(e.toString())
                    .slots(slots)
                    .build());
        });

        return ResponseEntity.ok().body(dateSlots);
    }

    private List<TimeBooking> genTimeBookingPicked(final Collection<DateTimeBooking> dateTimeBookings) {

        return dateTimeBookings.stream()
                .map(DateTimeBooking::getTimeBooking)
                .collect(Collectors.toList());
    }


    private List<LocalDate> getListDayofWeek(String dateFrom) {
        final LocalDate date = LocalDate.parse(dateFrom);
        date.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));

        return IntStream.range(0, 8).mapToObj(date::plusDays).collect(Collectors.toList());
    }
}
