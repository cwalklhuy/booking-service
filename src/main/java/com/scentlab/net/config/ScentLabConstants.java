package com.scentlab.net.config;

public class ScentLabConstants {
    public static final String WAIT_APPOINTMENT = "confirming";

    //TYPE Booking
    public static final String PERSONAL = "personal";
    public static final String FRIEND = "friend";
    public static final String COMPANY = "company";

    //STATUS Booking
    public static final String DEAL_CONFIRMING = "Đang đợi xác nhận từ chuyên gia";
    public static final String DEAL_CONFIRMED = "Chuyên gia đã ghi nhận";
    public static final String DEAL_PROCESSING = "Đang chờ xác nhận lại";
    public static final String PERFUME_TEST = "Thử nghiệm nước hoa";
    public static final String PERFUME_FINISHED = "Đã hoàn thành";

    //TYPE DateTimeBooking
    public static final String DATE_TIME_BOOKING_APPOINTMENT = "Đang có hẹn";
    public static final String DATE_TIME_BOOKING_FINISHED = "Đã hoàn thành";
    public static final String DATE_TIME_BOOKING_EXPERT_BUSY = "Chuyên gia bận";

    //SendGrid Template Ids
    public static final String SENDGRID_EXPERT_CONFIRMED_TEMPLATE_ID = "d-728c7ae6f851460cab2e2fbd21e0f67f";
}
