package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "perfume_fragrantica")
public class PerfumeFragrantica {

    @Id
    @Column(name = "perfume_fragrantica_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Long perfumeFragranticaId;

    @Column(name = "name")
    private String name;

    @Column(name = "vote")
    private Double vote;

    @Column(name = "voters")
    private Integer voters;

    @Column(name = "image")
    private String image;

    @Column(name = "longevity_poor")
    private Integer longevityPoor;

    @Column(name = "longevity_weak")
    private Integer longevityWeak;

    @Column(name = "longevity_moderate")
    private Integer longevityModerate;

    @Column(name = "longevity_longlasting")
    private Integer longevityLongLasting;

    @Column(name = "longevity_verylonglasting")
    private Integer longevityVeryLongLasting;

    @Column(name = "sillage_soft")
    private Integer sillageSoft;

    @Column(name = "sillage_moderate")
    private Integer sillageModerate;

    @Column(name = "sillage_heavy")
    private Integer sillageHeavy;

    @Column(name = "sillage_enormous")
    private Integer sillageEnorMous;

    @Column(name = "is_crawled")
    private Boolean isCrawled;

    @Column(name = "detail_link")
    private String detailLink;

    @Column(name = "gender")
    private Integer gender;

    @JsonIgnore
    @OneToMany(mappedBy = "perfumeFragrantica", fetch = FetchType.LAZY)
    private Collection<NotePerfume> notePerfumes;
}
