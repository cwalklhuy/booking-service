package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "date_booking")
@Builder
public class DateBooking {
    @Id
    @Column(name = "date_booking_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer dateBookingId;

    @Column(name = "date_created", length = 16)
    private LocalDate dateCreated;

    @JsonIgnore
    @OneToMany(mappedBy = "dateBooking", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<DateTimeBooking> dateTimeBookings;
}
