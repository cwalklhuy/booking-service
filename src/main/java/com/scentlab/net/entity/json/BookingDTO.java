package com.scentlab.net.entity.json;

import java.util.Collection;

import com.scentlab.net.entity.TimeBooking;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingDTO {

    private Integer bookingId;
    private String expert;
    private String status;
    private Integer rating;
    private String expertMessage;
    private String noteFromExpert;
    private String type;
    private String dateBooking;
    private String dateFinished = "...";
    private Integer timeBookingId;
    private String timeStart;
    private String timeEnd;
    private String gender;
    private String style;
    private String typePerfume;
    private Integer age;
    private String fee = "... VND";
    private Collection<PerfumeDTO> perfumes;

    public void setCustomerProfile(CustomerProfile profile) {
        this.gender = profile.getGender();
        this.style = profile.getStyle();
        this.typePerfume = profile.getTypePerfume();
        this.age = profile.getAge();
    }

    public void setTimeBooking(TimeBooking timeBooking) {
        this.timeBookingId = timeBooking.getTimeBookingId();
        this.timeStart = timeBooking.getTimeStart();
        this.timeEnd = timeBooking.getTimeEnd();
    }
}