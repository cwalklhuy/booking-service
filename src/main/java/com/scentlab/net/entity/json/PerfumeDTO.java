package com.scentlab.net.entity.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PerfumeDTO {

    private String perfumeFragranticaId;
    private String name;
    private String image;
}