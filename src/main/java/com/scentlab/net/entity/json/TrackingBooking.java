package com.scentlab.net.entity.json;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrackingBooking {
    private String date;
    private String time;
    private String notificationName;

}
