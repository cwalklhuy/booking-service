package com.scentlab.net.entity.json;

import com.scentlab.net.entity.Note;
import com.scentlab.net.entity.NotePerfume;
import com.scentlab.net.entity.PerfumeFragrantica;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PerfumePicked {

    private Long perfumeFragranticaId;
    private String name;
    private Double vote;
    private Integer voters;
    private String image;
    private Integer longevityPoor;
    private Integer longevityWeak;
    private Integer longevityModerate;
    private Integer longevityLongLasting;
    private Integer longevityVeryLongLasting;
    private Integer sillageSoft;
    private Integer sillageModerate;
    private Integer sillageHeavy;
    private Integer sillageEnorMous;
    private String detailLink;
    private Integer gender;
    private List<Note> notes;

    public PerfumePicked(PerfumeFragrantica perfumeFragrantica) {
        this.perfumeFragranticaId = perfumeFragrantica.getPerfumeFragranticaId();
        this.name = perfumeFragrantica.getName();
        this.vote = perfumeFragrantica.getVote();
        this.voters = perfumeFragrantica.getVoters();
        this.longevityPoor = perfumeFragrantica.getLongevityPoor();
        this.longevityWeak = perfumeFragrantica.getLongevityWeak();
        this.longevityModerate = perfumeFragrantica.getLongevityModerate();
        this.longevityLongLasting = perfumeFragrantica.getLongevityLongLasting();
        this.longevityVeryLongLasting = perfumeFragrantica.getLongevityVeryLongLasting();
        this.sillageSoft = perfumeFragrantica.getSillageSoft();
        this.sillageModerate = perfumeFragrantica.getSillageModerate();
        this.detailLink = perfumeFragrantica.getDetailLink();
        this.gender = perfumeFragrantica.getGender();
        this.notes = perfumeFragrantica.getNotePerfumes().stream()
                .map(NotePerfume::getNote)
                .collect(Collectors.toList());
    }
}
