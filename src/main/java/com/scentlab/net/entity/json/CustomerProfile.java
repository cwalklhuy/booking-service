package com.scentlab.net.entity.json;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    private String gender;
    private String style;
    private String volume;
    private String typePerfume;
    private Integer age;
}
