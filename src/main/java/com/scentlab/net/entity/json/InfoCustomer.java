package com.scentlab.net.entity.json;

import lombok.*;

import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InfoCustomer {
    private CustomerProfile customerProfile;
    private Collection<NoteAvg> noteAvgs;
    private Collection<PerfumePicked> perfumePickeds;
    private List<TrackingBooking> trackingBookings;
}
