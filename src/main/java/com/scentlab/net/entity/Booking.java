package com.scentlab.net.entity;

import com.scentlab.net.entity.json.InfoCustomer;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "booking")
public class Booking extends BaseEntity {

    @Id
    @Column(name = "booking_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer bookingId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
    @JoinColumn(name = "username", referencedColumnName = "username")
    private Account account;

    @Column(name = "status")
    private String status;

    @Column(name = "rating")
    private Integer rating;

    @Column(name = "expert_message")
    private String expertMessage;

    @Column(name = "note_from_expert")
    private String noteFromExpert;

    @Column(name = "type")
    private String type;

    @Column(name = "customer_name")
    private String customerName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @JoinColumn(name = "datetime_booking_id", referencedColumnName = "datetime_booking_id")
    private DateTimeBooking dateTimeBooking;

    @Column(name = "customer_attributes", columnDefinition = "json")
    @Type(type = "json")
    private InfoCustomer infoCustomer;

}
