package com.scentlab.net.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "datetime_booking")
@Builder
public class DateTimeBooking {
    @Id
    @Column(name = "datetime_booking_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer dateTimeBookingId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "date_booking_id", referencedColumnName = "date_booking_id")
    private DateBooking dateBooking;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "time_booking_id", referencedColumnName = "time_booking_id")
    private TimeBooking timeBooking;

    //Appointment
    //Busy
    @Column(name = "type", length = 16)
    private String type;

    @OneToMany(mappedBy = "dateTimeBooking")
    private Collection<Booking> bookings;

}
