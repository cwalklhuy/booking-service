package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "note")
public class Note {

    @Id
    @Column(name = "note_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Long noteId;

    @Column(name = "note", nullable = false, length = 200)
    private String name;

    @Column(name = "is_crawled")
    private Boolean isCrawled;

    //    @Transient
    @Column(name = "detail_link_of_note")
    private String detailLinkOfNote;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "group_note_id", referencedColumnName = "group_note_id")
    private GroupNote groupNote;

    @JsonIgnore
    @OneToMany(mappedBy = "note", fetch = FetchType.LAZY)
    private Collection<NotePerfume> notePerfumes;
}
