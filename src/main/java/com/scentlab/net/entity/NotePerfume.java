package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "note_perfume")
public class NotePerfume {

    @EmbeddedId
    private NotePerfumePK notePerfumePK;

    @JsonIgnore
    @MapsId("noteId")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "note_id", referencedColumnName = "note_id")
    private Note note;

    @JsonIgnore
    @MapsId("perfumeFragranticaId")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "perfume_fragrantica_id", referencedColumnName = "perfume_fragrantica_id")
    private PerfumeFragrantica perfumeFragrantica;
}
