package com.scentlab.net.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "group_note")
public class GroupNote {

    @Id
    @Column(name = "group_note_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer groupNoteId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "groupNote", fetch = FetchType.LAZY)
    private Collection<Note> notes;
}
