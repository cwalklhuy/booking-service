package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "time_booking")
public class TimeBooking {

    @Id
    @Column(name = "time_booking_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer timeBookingId;

    @Column(name = "time_start")
    private String timeStart;

    @Column(name = "time_end")
    private String timeEnd;

    @JsonIgnore
    @OneToMany(mappedBy = "timeBooking", fetch = FetchType.LAZY)
    private Collection<DateTimeBooking> dateTimeBookings;
}
