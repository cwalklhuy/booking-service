package com.scentlab.net.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class NotePerfumePK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column
    private Long perfumeFragranticaId;

    @Column
    private Long noteId;

    @Override
    public int hashCode() {
        return Objects.hash(perfumeFragranticaId, noteId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        NotePerfumePK notePerfumePK = (NotePerfumePK) obj;
        return perfumeFragranticaId.equals(notePerfumePK.perfumeFragranticaId) && noteId.equals(notePerfumePK.noteId);
    }
}
