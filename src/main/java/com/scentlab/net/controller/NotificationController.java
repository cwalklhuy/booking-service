package com.scentlab.net.controller;

import com.scentlab.net.payload.response.NotificationExpert;
import com.scentlab.net.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("notifications")
public class NotificationController {

    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/{username}")
    public ResponseEntity<List<NotificationExpert>> getNotificationByUsername(
            @PathVariable(value = "username") String username) {

        return notificationService.getNotificationByUsername(username);
    }

}
