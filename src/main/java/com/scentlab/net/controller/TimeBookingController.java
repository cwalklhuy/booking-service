package com.scentlab.net.controller;

import java.util.List;

import com.scentlab.net.payload.response.DateSlots;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scentlab.net.entity.TimeBooking;
import com.scentlab.net.service.TimeBookingService;

@RestController
@RequestMapping("timebookings")
public class TimeBookingController {

    private final TimeBookingService timeBookingService;

    public TimeBookingController(TimeBookingService timeBookingService) {
        this.timeBookingService = timeBookingService;
    }

    @GetMapping("/{datePicked}")
    public ResponseEntity<List<TimeBooking>> getTimeBookingAvailable(
            @PathVariable(value = "datePicked") String datePicked) {

        return timeBookingService.getTimeBookingAvailable(datePicked);
    }

    @GetMapping("/{dateFrom}/{dateTo}")
    public ResponseEntity<List<DateSlots>> getTimeBookingOneWeek(@PathVariable(value = "dateFrom") String dateFrom,
            @PathVariable(value = "dateTo") String dateTo) {

        return timeBookingService.getTimeBookingOneWeek(dateFrom, dateTo);
    }
}
