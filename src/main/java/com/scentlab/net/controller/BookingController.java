package com.scentlab.net.controller;

import com.scentlab.net.payload.request.MakeBookingReg;
import com.scentlab.net.payload.request.UpdateBookingReq;
import com.scentlab.net.payload.response.CustomerBooking;
import com.scentlab.net.service.BookingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("bookings")
public class BookingController {

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping
    public Collection<?> getAllBookings() {

        return bookingService.getAllBookings();
    }

    @PostMapping("/create")
    public ResponseEntity<?> createBooking(@RequestBody MakeBookingReg makeBooking) {

        return bookingService.createBooking(makeBooking);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateBooking(@RequestBody UpdateBookingReq updateBookingReq){

        return bookingService.updateBooking(updateBookingReq);
    }

    @GetMapping("/customers")
    public List<?> findAllBookings() {

        return bookingService.findAllBookings();
    }

    @GetMapping("/finished/{bookingId}")
    public ResponseEntity<?> bookingDone(@PathVariable(value = "bookingId") Integer bookingId) {

        return bookingService.bookingDone(bookingId);
    }

    @GetMapping("/customers/{customerName}")
    public ResponseEntity<List<CustomerBooking>> findAllBookings(
            @PathVariable(value = "customerName") String customerName) {

        return bookingService.findAllBookingsByCustomerName(customerName);
    }

    @GetMapping("/rating/{bookingId}")
    public ResponseEntity<?> rating(@PathVariable(value = "bookingId") Integer bookingId,
            @RequestParam(defaultValue = "0") Integer rating) {

        return bookingService.rating(rating, bookingId);
    }

    @GetMapping("/{bookingId}")
    public ResponseEntity<CustomerBooking> findBookingByBookingId(
            @PathVariable(value = "bookingId") Integer bookingId) {

        return bookingService.findBookingByBookingId(bookingId);
    }
}
