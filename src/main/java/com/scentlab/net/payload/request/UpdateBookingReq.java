package com.scentlab.net.payload.request;

import java.util.List;

import com.scentlab.net.entity.json.CustomerProfile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateBookingReq {
	private Integer bookingId;
	private String customerName;
    private String makeAppointmentDate;
    private String timeBookingId;
    private String expertMessage;
    private String noteFromExpert;
    private String type;
    private String status;
    private CustomerProfile customerProfile;
    private List<Long> perfumeId;
}
