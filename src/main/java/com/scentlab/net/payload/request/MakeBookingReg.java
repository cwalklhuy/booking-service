package com.scentlab.net.payload.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.scentlab.net.entity.json.CustomerProfile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MakeBookingReg {
    private String userId;
    private String customerName;
    private String makeAppointmentDate;
    private String timeBookingId;
    private String expertMessage;
    private String noteFromExpert;
    private String type;
    private String status;
    private CustomerProfile customerProfile;
    private List<Long> perfumeId;
}
