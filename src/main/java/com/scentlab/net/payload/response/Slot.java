package com.scentlab.net.payload.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Slot {
    private Integer timeBookingId;
    private String status;

}
