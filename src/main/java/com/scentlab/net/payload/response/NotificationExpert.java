package com.scentlab.net.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NotificationExpert implements Comparable<NotificationExpert>{
    private Integer bookingId;
    private String date;
    private String time;
    private String notification;

    @Override
    public int compareTo(NotificationExpert o) {
        return Comparator.comparing(NotificationExpert::getDate)
                .thenComparing(NotificationExpert::getTime)
                .compare(o, this);
    }
}
