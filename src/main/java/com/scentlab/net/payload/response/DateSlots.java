package com.scentlab.net.payload.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DateSlots {
    private String date;
    private List<Slot> slots;
}
