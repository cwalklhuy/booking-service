package com.scentlab.net.payload.response;

import com.scentlab.net.entity.Booking;
import com.scentlab.net.entity.json.InfoCustomer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerBooking {
	private String bookingId;
	private String username;
	private String fullName;
	private String type;
	private String status;
	private String appointmentDate;
	private String timeStart;
	private String timeEnd;
	private String expertMessage;
	private String noteFromExpert;
	private InfoCustomer infoCustomer;

	public CustomerBooking(Booking booking) {
		this.bookingId = String.valueOf(booking.getBookingId());
		this.username = genUsername(booking);
		this.fullName = genFullName(booking);
		this.type = booking.getType();
		this.status = booking.getStatus();
		this.appointmentDate = booking.getDateTimeBooking().getDateBooking().getDateCreated().toString();
		this.timeStart = booking.getDateTimeBooking().getTimeBooking().getTimeStart();
		this.timeEnd = booking.getDateTimeBooking().getTimeBooking().getTimeEnd();
		this.expertMessage = booking.getExpertMessage();
		this.noteFromExpert = booking.getNoteFromExpert();
		this.infoCustomer = booking.getInfoCustomer();
	}

	private String genUsername(Booking booking) {

	    if (booking.getAccount() != null) {
	        return booking.getAccount().getUsername();
        }

	    return "";
    }

    private String genFullName(Booking booking) {
		if (booking.getCustomerName() == null) {
			return "";
		}

	    return booking.getCustomerName();
    }
}
