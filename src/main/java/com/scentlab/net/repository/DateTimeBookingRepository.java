package com.scentlab.net.repository;

import com.scentlab.net.entity.DateBooking;
import com.scentlab.net.entity.DateTimeBooking;
import com.scentlab.net.entity.TimeBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface DateTimeBookingRepository extends JpaRepository<DateTimeBooking, Integer> {
    Collection<DateTimeBooking> findAllByDateBookingDateCreated(LocalDate dateCreated);

    @Query("select p from datetime_booking p where p.dateBooking.dateBookingId = ?1 and p.timeBooking.timeBookingId = ?2")
    Optional<DateTimeBooking> findByDateBookingIdAndTimeBookingId(Integer dateBookingId, Integer timeBookingId);

    List<DateTimeBooking> findByDateBookingAndTimeBooking(DateBooking dateBooking, TimeBooking timeBooking);

    List<DateTimeBooking> findAllByDateBooking_DateCreatedAndTimeBooking_TimeBookingId(LocalDate dateBooking, Integer timeBookingId);
}
