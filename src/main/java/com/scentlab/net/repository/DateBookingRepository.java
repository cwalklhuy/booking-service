package com.scentlab.net.repository;

import com.scentlab.net.entity.DateBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DateBookingRepository extends JpaRepository<DateBooking, Integer> {

   List<DateBooking> findDateBookingByDateCreated(LocalDate dateCreated);
}
