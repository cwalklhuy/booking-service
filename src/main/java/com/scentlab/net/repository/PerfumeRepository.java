package com.scentlab.net.repository;

import com.scentlab.net.entity.PerfumeFragrantica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerfumeRepository extends JpaRepository<PerfumeFragrantica, Integer> {

    @Query("select p from PerfumeFragrantica p where p.perfumeFragranticaId in :ids")
    List<PerfumeFragrantica> findByPerfumeFragranticaIdIn(@Param("ids") List<Long> perfumeFrangranticaIds);

}
