package com.scentlab.net.repository;

import com.scentlab.net.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {

    Collection<Booking> findAllByAccount_Username(String username);

}
