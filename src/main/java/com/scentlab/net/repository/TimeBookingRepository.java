package com.scentlab.net.repository;

import com.scentlab.net.entity.TimeBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeBookingRepository extends JpaRepository<TimeBooking, Integer> {
}
