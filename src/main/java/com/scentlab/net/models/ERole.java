package com.scentlab.net.models;

public enum ERole {
    ROLE_USER,
    ROLE_EXPERT,
    ROLE_ADMIN
}
